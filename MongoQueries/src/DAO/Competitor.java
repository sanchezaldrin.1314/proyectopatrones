/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.ArrayList;

/**
 *
 * @author Estalin
 */
public class Competitor extends Client {
    private ArrayList<Game> games;

    public Competitor(ArrayList<Game> games, String _id, String client_name, String last_name, String picture, String user_code, boolean is_competitot) {
        super(_id, client_name, last_name, picture, user_code, is_competitot);
        this.games = games;
    }

    public ArrayList<Game> getGames() {
        return games;
    }

    public void setGames(ArrayList<Game> games) {
        this.games = games;
    }
    
}
