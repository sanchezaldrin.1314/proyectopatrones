/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author Estalin
 */
public class Client {
    private String _id;

    private String client_name;
    private String last_name;
    private String picture;
    private String user_code;
    private boolean is_competitor;

    public Client(String _id, String client_name, String last_name, String picture, String user_code, boolean is_competitor) {
        this._id = _id;
        this.client_name = client_name;
        this.last_name = last_name;
        this.picture = picture;
        this.user_code = user_code;
        this.is_competitor = is_competitor;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUser_code() {
        return user_code;
    }

    public void setUser_code(String user_code) {
        this.user_code = user_code;
    }

    public boolean isIs_competitor() {
        return is_competitor;
    }

    public void setIs_competitor(boolean is_competitot) {
        this.is_competitor = is_competitot;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }
    
}
