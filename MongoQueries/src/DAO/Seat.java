/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author Estalin
 */
public class Seat {
    private String _id;
    private String seat_name;
    private String address;
    private String seat_description;

    public Seat(String _id, String seat_name, String address, String seat_description) {
        this._id = _id;
        this.seat_name = seat_name;
        this.address = address;
        this.seat_description = seat_description;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getSeat_name() {
        return seat_name;
    }

    public void setSeat_name(String seat_name) {
        this.seat_name = seat_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSeat_description() {
        return seat_description;
    }

    public void setSeat_description(String seat_description) {
        this.seat_description = seat_description;
    }
    
}
