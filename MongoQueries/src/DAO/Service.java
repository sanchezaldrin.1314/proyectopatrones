/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author Estalin
 */
public class Service {
    private String _id;
    private String service_name;
    private String service_description;
    private String seat_id;

    public Service(String _id, String service_name, String service_description, String seat_id) {
        this._id = _id;
        this.service_name = service_name;
        this.service_description = service_description;
        this.seat_id = seat_id;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }

    public String getSeat_id() {
        return seat_id;
    }

    public void setSeat_id(String seat_id) {
        this.seat_id = seat_id;
    }
    
    
}
