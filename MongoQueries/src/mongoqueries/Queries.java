/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mongoqueries;

import DAO.Client;
import DAO.Seat;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.eq;
import java.util.ArrayList;
import org.bson.Document;

/**
 *
 * @author Estalin
 */
public class Queries {

    private static final MongoDatabase db = MongoConnection.getMongoDatabase();

    public Client login(String user_code) {
        Document existingUser = db.getCollection("client").find(eq("user_code", user_code)).first();
        if (existingUser != null) {
            String _id = existingUser.getObjectId("_id").toString();
            String client_name = existingUser.getString("client_name");
            String last_name = existingUser.getString("last_name");
            String picture = existingUser.getString("client_name");
            boolean is_competitor = existingUser.getBoolean("is_competitor");
            Client existingClient = new Client(_id, client_name, last_name, picture, user_code, is_competitor);
            return existingClient;
        }
        return null;
    }

    public ArrayList<Seat> listarSedes() {
        ArrayList<Seat> sedes = new ArrayList<>();
        try (MongoCursor<Document> cursor = db.getCollection("seats").find().iterator()) {
            String _id;
            String seat_name;
            String address;
            String seat_description;
            Seat newSeat;
            while (cursor.hasNext()) {
                _id = cursor.next().getObjectId("_id").toString();
                seat_name = cursor.next().getString("seat_name");
                address = cursor.next().getString("address");
                seat_description = cursor.next().getString("seat_description");
                newSeat = new Seat(_id, seat_name, address, seat_description);
                sedes.add(newSeat);
            }
        }
        return null;
    }
}
