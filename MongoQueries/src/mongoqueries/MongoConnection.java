/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mongoqueries;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

/**
 *
 * @author Estalin
 */
public class MongoConnection {
    private static final String user = "admin";
    private static final String password = "admin123";
    private static final String host = "ds135207.mlab.com:35207/";
    private static final String dbname = "panamericangames";
    private static MongoClient mongoClient = null;
    private static MongoDatabase db = null;

    private MongoConnection() {}

    public static MongoDatabase getMongoDatabase() {
        if(mongoClient==null){
            mongoClient = new MongoClient(new MongoClientURI("mongodb://" + user + ":" + password + "@" + host + dbname));
        }
        if (db == null) {
            System.out.println("******* Trying to get Mongo DB - panamericangames - connection. *******");
            db = mongoClient.getDatabase("panamericangames");;
            System.out.println("******* Successfully Connected to - panamericangames - Mongo DB *******");
        }
        return db;
    }
    
}
